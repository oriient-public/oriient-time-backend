const express = require('express');

const app = express();
const port = 8080;

let offset = 0;

app.use(express.json())

app.get('/api/time', (req, res) => {
    res.status(200).send({time: Date.now() + offset});
})

app.post('/api/time/offset', (req, res) => {
    if(!req.body.offset){
        res.sendStatus(400)
    }else{
        offset += req.body.offset;
        res.status(200).send({time: Date.now() + offset});
    }
})

app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`)
})
